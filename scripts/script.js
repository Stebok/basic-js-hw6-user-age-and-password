function createNewUser() {
  let userFirstName = prompt("Enter your first name: ");
  let usrLastName = prompt("Enter your last name: ");
  let date = prompt("Enter your date of birth as dd.mm.yyyy");

  let dirthDate = date.split(".")[0];
  let birthMonth = date.split(".")[1];
  let birthYear = date.split(".")[2];

  let userDateOfBirth = new Date(birthYear, birthMonth, dirthDate);
  console.log(userDateOfBirth);

  let newUser = {
    firstName: userFirstName,
    lastName: usrLastName,
    birthday: userDateOfBirth,
    getLogin: function () {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },

    getAge: function () {
      let currentDate = new Date();
      let userAge = Math.floor(
        (currentDate - this.birthday) / (1000 * 60 * 60 * 24 * 365)
      );
      return userAge;
    },
    getPassword: function () {
      let splitted = userFirstName.split("");
      let firstLetter = splitted[0].toUpperCase();
      return (
        firstLetter +
        `${this.lastName}${this.birthday.getFullYear()}`.toLowerCase()
      );
    },
  };
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
